const fs = require("fs");
const readline = require("readline");
const path = require("path");

const csvFilePath =
"D:\MangoDB\StockEtablissement\StockEtablissement_utf8.csv";
const outputDir = "./output/";

const maxFileSize = 500 * 1024 * 1024; // 500 Mo

async function decomposeAndCleanCSV(csvFilePath, outputDir, maxFileSize) {
  const fileStream = fs.createReadStream(csvFilePath);
  const rl = readline.createInterface({
    input: fileStream,
    crlfDelay: Infinity,
  });

  let currentFileSize = 0;
  let fileNumber = 1;
  let outputFileStream = null;

  for await (const line of rl) {
    if (!outputFileStream || currentFileSize + line.length > maxFileSize) {
      if (outputFileStream) {
        outputFileStream.end();
      }
      const outputFileName = path.join(outputDir, `output_${fileNumber}.csv`);
      outputFileStream = fs.createWriteStream(outputFileName);
      fileNumber++;
      currentFileSize = 0;
    }

    // Vérifier si la ligne contient des données vides
    if (line.trim() !== "") {
      outputFileStream.write(line + "\n");
      currentFileSize += line.length;
    }
  }

  if (outputFileStream) {
    outputFileStream.end();
  }
}

decomposeAndCleanCSV(csvFilePath, outputDir, maxFileSize)
  .then(() => {
    console.log("Décomposition et nettoyage terminés avec succès.");
  })
  .catch((err) => {
    console.error(
      "Erreur lors de la décomposition et du nettoyage du fichier CSV :",
      err
    );
  });