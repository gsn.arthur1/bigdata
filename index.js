const fs = require('fs');
const csv = require('csv-parser');

const { MongoClient } = require('mongodb');
const uri = "mongodb://localhost:27017/"; // URL de connexion à MongoDB
const dbName = "Ensitech"; // Nom de votre base de données MongoDB
const collectionName = "EnsitechBigData";

const inputFile = 'D:\\MangoDB\\StockEtablissement\\StockEtablissement_utf8.csv';
const outputFolder = './output/';
const jsonOutputFolder = './outputJSON/';
const chunkSize = 1000000; // Nombre de lignes par chunk

// Créer un dossier de sortie s'il n'existe pas déjà
if (!fs.existsSync(outputFolder)) {
    fs.mkdirSync(outputFolder);
}

let currentChunk = 0;
let linesCount = 0;
let outputStream;
let startTime;
let paused = false;

const client = new MongoClient(uri);

async function connectToMongoDB() {
    try {
        await client.connect();
        console.log("Connecté à MongoDB avec succès !");
    } catch (error) {
        console.error("Erreur lors de la connexion à MongoDB :", error);
    }
}

// Vérifier si le dossier de sortie JSON existe, sinon le créer
if (!fs.existsSync(jsonOutputFolder)) {
    fs.mkdirSync(jsonOutputFolder);
}

// Parcourir tous les fichiers du dossier de sortie
fs.readdir(outputFolder, (err, files) => {
    if (err) {
        console.error("Erreur lors de la lecture du dossier de sortie :", err);
        return;
    }

    // Filtrer les fichiers avec l'extension .csv
    const csvFiles = files.filter(file => file.endsWith('.csv'));

    // Pour chaque fichier CSV, convertir en JSON
    csvFiles.forEach(csvFile => {
        const csvFilePath = `${outputFolder}/${csvFile}`;
        const jsonFilePath = `${jsonOutputFolder}/${csvFile.replace('.csv', '.json')}`;

        const jsonData = [];

        fs.createReadStream(csvFilePath)
            .pipe(csv())
            .on('data', (row) => {
                jsonData.push(row);
            })
            .on('end', () => {
                fs.writeFileSync(jsonFilePath, JSON.stringify(jsonData, null, 2));
                console.log(`Conversion de ${csvFile} en ${jsonFilePath} terminée.`);
            });
    });
});
/* 
// Fonction pour lire et insérer chaque chunk dans MongoDB
async function insertChunksIntoMongoDB() {

    try {
        await client.connect(); // Se connecter à MongoDB
        console.log("Connecté à MongoDB avec succès !");

        const db = client.db(dbName); // Sélectionner la base de données

        // Pour chaque fichier ou source de données contenant un chunk
        for (let i = 1; i <= 39; i++) {
            const chunkData = lireChunkDepuisSourceDeDonnees(i); // Lire le chunk depuis la source de données
            const collection = db.collection(collectionName); // Sélectionner la collection MongoDB

            await collection.insertMany(chunkData); // Insérer le chunk dans MongoDB
            console.log(`Chunk ${i} inséré avec succès dans la collection.`);
        }

        console.log("Tous les chunks ont été insérés avec succès dans la collection MongoDB.");
    } catch (error) {
        console.error("Erreur lors de l'insertion des chunks dans MongoDB :", error);
    } finally {
        await client.close(); // Fermer la connexion à MongoDB
        console.log("Connexion à MongoDB fermée avec succès !");
    }
}

// Fonction pour lire un chunk depuis la source de données
function lireChunkDepuisSourceDeDonnees(numero_chunk) {
    const nom_fichier = `output/chunk_${numero_chunk}.json`;
    const chunkData = fs.readFileSync(nom_fichier); // Lire le fichier du chunk
    return JSON.parse(chunkData); // Retourner les données du chunk
}

// Fonction pour mettre en pause le traitement
function pauseProcessing() {
  paused = true;
  console.log("Traitement en pause...");
}

// Fonction pour reprendre le traitement
function resumeProcessing() {
  paused = false;
  console.log("Reprise du traitement...");
}

// Gestionnaire d'événements pour le signal SIGINT (Ctrl+C)
process.on('SIGINT', () => {
  if (paused) {
    resumeProcessing();
  } else {
    pauseProcessing();
  }
});

// Fonction principale pour lire et insérer les données dans MongoDB
async function processData() {
  
  try {
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection(collectionName);

    fs.createReadStream('data.csv')
      .pipe(csv())
      .on('data', async (row) => {
        if (!paused) {
          await collection.insertOne(row); // Insérer les données dans MongoDB
          console.log("Données insérées :", row);
        } else {
          console.log("Traitement en pause...");
        }
      })
      .on('end', () => {
        console.log("Traitement terminé.");
        client.close();
      });
  } catch (error) {
    console.error("Erreur lors du traitement des données :", error);
  }
}

// Appel de la fonction principale pour démarrer le traitement
processData();
 */
/* 
// Fonction pour écrire une ligne dans le fichier de sortie
function writeLine(line) {
    outputStream.write(line + '\n');
    linesCount++;

    // Si le nombre de lignes atteint le chunkSize, passer au fichier suivant
    if (linesCount >= chunkSize) {
        outputStream.end();
        const endTime = new Date();
        const duration = endTime - startTime;
        console.log(`Chunk ${currentChunk} généré en ${duration} ms.`);
        currentChunk++;
        outputStream = fs.createWriteStream(`${outputFolder}/chunk_${currentChunk}.csv`);
        linesCount = 0;
        startTime = new Date();
    }
}

// Lire le fichier CSV ligne par ligne et écrire dans les chunks
fs.createReadStream(inputFile)
    .pipe(csv())
    .on('data', (row) => {
        if (!outputStream) {
            outputStream = fs.createWriteStream(`${outputFolder}/chunk_${currentChunk}.csv`);
            startTime = new Date();
        }
        writeLine(Object.values(row).join(','));
    })
    .on('end', () => {
        if (outputStream) {
            outputStream.end();
            const endTime = new Date();
            const duration = endTime - startTime;
            console.log(`Chunk ${currentChunk} généré en ${duration} ms.`);
        }
        console.log('Fichier divisé en chunks avec succès.');
    });
     */