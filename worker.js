const pm2 = require('pm2');

// Configuration des options de démarrage des workers
const workerOptions = {
    script: 'index.js', // Chemin vers le script du worker
    instances: 4, // Nombre d'instances de workers
    name: 'workerBigData' // Nom du worker
};

// Démarrer les workers avec PM2
pm2.connect((err) => {
    if (err) {
        console.error('Erreur lors de la connexion à PM2 :', err);
        process.exit(1);
    }

    pm2.start(workerOptions, (err, apps) => {
        if (err) {
            console.error('Erreur lors du démarrage des workers avec PM2 :', err);
            pm2.disconnect();
            process.exit(1);
        }

        console.log('Workers démarrés avec succès.');

        // Écouter les événements des workers
        pm2.launchBus((err, bus) => {
            if (err) {
                console.error('Erreur lors de l\'écoute des événements PM2 :', err);
                pm2.disconnect();
                process.exit(1);
            }

            bus.on('log:out', (packet) => {
                // Gérer les logs des workers
                console.log('Log des workers :', packet.data);
            });

            bus.on('process:event', (packet) => {
                // Gérer les événements des workers
                console.log('Événement des workers :', packet);
            });
        });
    });
});